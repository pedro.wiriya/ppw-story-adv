from django.contrib import auth
from django.contrib.auth.models import User
from django.http import response
from django.test import TestCase , Client
from django.urls import resolve
from .views import register

class StoryApp9Test(TestCase):
    
    def test_login_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_logout_url(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 200)

    def test_register_url(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_view(self):
        response = resolve('/register/')
        self.assertEqual(response.func, register)

    def test_login_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'login.html')

    def test_logout_template(self):
        response = Client().get('/logout/')
        self.assertTemplateUsed(response, 'logout.html')

    def test_register_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'register.html')

    def test_register(self):
        response = Client().post('/register/', data={'first': 'first', 'last': 'last', 'email' : 'asd@gmail.com','username':'hehehe', 'password1' : 'agfeoqwegnhoqwgbqn', 'password2' : 'agfeoqwegnhoqwgbqn'}, format='text/html')
        amount = User.objects.count()
        self.assertEqual(amount, 1)