from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import FormRegister

def register(request):
    if request.method == 'POST':
        form = FormRegister(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account Created for {username}!')
            return redirect('story_app_9:login')
    else:
        form = FormRegister()
    context = {'form' : form}
    return render(request, 'register.html', context)
