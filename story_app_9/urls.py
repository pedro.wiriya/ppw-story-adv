from django.urls import path
from django.contrib.auth import views as auth
from .views import register

app_name ='story_app_9'
urlpatterns = [
    path('', auth.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth.LogoutView.as_view(template_name='logout.html'), name='logout'),
    path('register/', register, name='register')
]
