from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import widgets, TextInput, EmailInput, PasswordInput

class FormRegister(UserCreationForm):
    first = forms.CharField(widget=TextInput(attrs={'class':'form-control form-control-sm', 'required': True}))
    last = forms.CharField(widget=TextInput(attrs={'class':'form-control form-control-sm', 'required': True}))
    email = forms.EmailField(widget=EmailInput(attrs={'class':'form-control form-control-sm', 'required': True}))

    class Meta:
        model = User
        fields = ['first', 'last', 'username', 'email', 'password1', 'password2']

        widgets = {
            'username' : TextInput(attrs={'class':'form-control form-control-sm', 'required': True}),
            'password1' : PasswordInput(attrs={'class':'form-control form-control-sm', 'required': True}),
            'password2' : PasswordInput(attrs={'class':'form-control form-control-sm', 'required': True})
        }