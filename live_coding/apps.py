from django.apps import AppConfig


class LiveCodingConfig(AppConfig):
    name = 'live_coding'
