from django.test import TestCase, Client    
from django.urls import resolve
from .views import home, cv, demo, aboutme, form, display, detail
from .models import MataKuliah

# Create your tests here.
class StoryOneToFiveTest(TestCase):

    def test_url_1(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_url_2(self):
        response = Client().get('/cv/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_3(self):
        response = Client().get('/about-me/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_4(self):
        response = Client().get('/demo/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_5(self):
        response = Client().get('/forms/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_6(self):
        response = Client().get('/display/')
        self.assertEqual(response.status_code, 200)
    
    def test_views_2(self):
        get = resolve('/cv/')
        self.assertEqual(get.func, cv)
    
    def test_views_3(self):
        get = resolve('/about-me/')
        self.assertEqual(get.func, aboutme)
    
    def test_views_4(self):
        get = resolve('/demo/')
        self.assertEqual(get.func, demo)

    def test_views_5(self):
        get = resolve('/forms/')
        self.assertEqual(get.func, form)
    
    def test_views_6(self):
        get = resolve('/display/')
        self.assertEqual(get.func, display)
    
    def test_template_1(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response, 'home.html')

    def test_template_2(self):
        response = Client().get('/cv/')
        self.assertTemplateUsed(response, 'cv.html')
    
    def test_template_3(self):
        response = Client().get('/about-me/')
        self.assertTemplateUsed(response, 'about_me.html')
    
    def test_template_4(self):
        response = Client().get('/demo/')
        self.assertTemplateUsed(response, 'number.html')
    
    def test_template_5(self):
        response = Client().get('/forms/')
        self.assertTemplateUsed(response, 'forms.html')
    
    def test_template_6(self):
        response = Client().get('/display/')
        self.assertTemplateUsed(response, 'data_display.html')

    def test_model(self):
        my_data = {
            'mata_kuliah' : 'SDA',
            'dosen' : 'Mei Silviana Saputri',
            'jumlah_sks' : 4,
            'semester' : 3,
            'kelas' : 'C',
            'description' :'Mata kuliah Struktur Data dan Algoritma'
        }
        response = Client().post('/forms/', data = my_data)
        amount = MataKuliah.objects.filter(mata_kuliah = 'SDA').count()
        self.assertEqual(amount, 1)