from django.urls import path
from .views import home, cv, demo, aboutme, form, display, detail, delete_view

urlpatterns = [
    path('home/', home, name='home'),
    path('cv/', cv, name='cv'),
    path('about-me/', aboutme, name='about-me'),
    path('demo/', demo, name='demo'),
    path('forms/', form, name='form'),
    path('display/', display, name='display'),
    path('display/<int:my_id>/', detail, name='detail'),
    path('display/<int:my_id>/delete', delete_view, name='delete')
]