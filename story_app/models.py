from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    mata_kuliah = models.CharField(max_length = 100)
    dosen = models.CharField(max_length = 100)
    jumlah_sks = models.PositiveSmallIntegerField()
    semester = models.PositiveSmallIntegerField()
    kelas = models.CharField(max_length = 100)
    description = models.TextField()

    def __str__(self): 
        return self.mata_kuliah