$( document ).ready(function(){
    $("#search").keyup(function(){
        var cari = $("#search").val();
        console.log(cari);
        $.ajax({
            url: "/search/"+cari,
            contentType:"application/json",
            dataType:'json',
            success: function(hasil) {
               $("#resultsearch").empty();
                for (var i = 0; i < hasil.items.length; i++) {
                    console.log(hasil.items.length);
                    var isi = "";
                    isi += '<div class="card border-dark mb-3" style="max-width: 30rem;">';
                    if (typeof hasil.items[i].volumeInfo.imageLinks === 'undefined') {
                        isi += '<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2dM2rpp1m8GOXl9CEKJ5KrQEA7-2ihbmRFg&usqp=CAU" class="card-img-top" alt="">'
                    } else {
                        isi += '<img src='+ hasil.items[i].volumeInfo.imageLinks.thumbnail + 'class="card-img-top" alt="">';
                    }
                    isi += '<div class="card-body">';
                    isi += '<p style="text-align : left; font-size : 0.5em"> Title : '+ hasil.items[i].volumeInfo.title +'</p>';
                    isi += '<p style="text-align : left; font-size : 0.5em"> Publisher : '+ hasil.items[i].volumeInfo.publisher +'</p>';
                    isi += '<div class="card-footer">';
                    isi += '<a style="text-align : center;" href=' + hasil.items[i].volumeInfo.previewLink  +'target="_blank">Visit</a>';
                    isi += '</div>';
                    isi += '</div>';
                    $("#resultsearch").append(isi);
                }
            }
        });
    });
})