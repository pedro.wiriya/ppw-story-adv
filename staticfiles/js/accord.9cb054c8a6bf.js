$( document ).ready(function() {
        $(".expand").click(function() {
            var class_select_content = $(this).parents(".accordion").find(".accordion-content"); //For Customizing Content
            var class_select_title = $(this).parents(".accordion-title") // For Customizing Title 

            if (class_select_content.css("display") == "none") {
                // Changing Display Type
                class_select_content.css("display","block");

                // Changing Border properties
                class_select_title.css("border-bottom-left-radius","0px");
                class_select_title.css("border-bottom-right-radius","0px");

                // Changing Icon
                class_select_title.find(".change").attr("src","/static/img/minus.svg");


            } else if (class_select_content.css("display") == "block") {
                // Changing Display Type
                class_select_content.css("display","none");

                // Changing Border properties
                class_select_title.css("border-bottom-left-radius","15px");
                class_select_title.css("border-bottom-right-radius","15px");

                // Changing Icon
                class_select_title.find(".change").attr("src","/static/img/plus.svg");
            }
        })

        $(".down").click(function() {
            var class_select_content = $(this).parents(".accordion");
            class_select_content.next().insertBefore(class_select_content);
        })

        $(".up").click(function() {
            var class_select_content = $(this).parents(".accordion");
            class_select_content.prev().insertAfter(class_select_content);
        })

});

