from django.http import request
from django.shortcuts import render, redirect
from .forms import OrangForm, KegiatanForm
from .models import Orang, Kegiatan

def register_activities(request):
    form = KegiatanForm(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return redirect('activities-display')
    context = {
        'page_title' : 'Register Your Activities',
        'form' : form
    }
    return render(request, 'register_activities.html', context)

def register_people(request):
    form = OrangForm(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return redirect('activities-display')
    context = {
        'page_title' : 'Register your people here',
        'form' : form
    }
    return render(request, 'register_people.html', context)

def display_activities(request):
    people = Orang.objects.all()
    activities = Kegiatan.objects.all()
    context = {
        'people' : people,
        'activities' : activities,
        'page_title' : 'Activities'
    }
    return render(request, 'activities_display.html', context)


