from django.test import TestCase, Client
from django.urls import resolve
from .views import display_activities, register_activities, register_people
from .models import Orang, Kegiatan
import datetime

class Story6TestTest(TestCase):

    def test_url_exist_1(self):
        response = Client().get('/activities/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_exist_2(self):
        response = Client().get('/activities/register-activities/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_exist_3(self):
        response = Client().get('/activities/register-people/')
        self.assertEqual(response.status_code, 200)
    
    def test_views_exist_1(self):
        get = resolve('/activities/')
        self.assertEqual(get.func, display_activities)

    def test_views_exist_2(self):
        get = resolve('/activities/register-activities/')
        self.assertEqual(get.func, register_activities)
    
    def test_views_exist_3(self):
        get = resolve('/activities/register-people/')
        self.assertEqual(get.func, register_people)
    
    def test_template_used_1(self):
        response = Client().get('/activities/')
        self.assertTemplateUsed(response, 'activities_display.html')
    
    def test_template_used_2(self):
        response = Client().get('/activities/register-activities/')
        self.assertTemplateUsed(response, 'register_activities.html')
    
    def test_template_used_3(self):
        response = Client().get('/activities/register-people/')
        self.assertTemplateUsed(response, 'register_people.html')
    
    def setUp(self):
        self.activites = Kegiatan.objects.create(name="BANZAIIII",date=datetime.date(2020,5,6))
        Orang.objects.create(full_name="Ren Amamiya", nickname="Joker", gender="Male", birthday=datetime.date(2000,5,13), activity = self.activites)
        Orang.objects.create(full_name="Makoto Nijima", nickname="Queen", gender="Female", birthday=datetime.date(1998,4,23), activity = self.activites)

    def test_if_HTML_display_models(self):
        response = Client().get('/activities/')
        html_response = response.content.decode('utf8')
        self.assertIn("BANZAIIII", html_response)
        self.assertIn("Ren Amamiya", html_response)
    
    def test_saving_a_POST_activities_request(self):
        response = Client().post('/activities/register-activities/', data={'name': 'Lunch', 'date': '2020-12-12'})
        amount = Kegiatan.objects.filter(name="Lunch").count()
        self.assertEqual(amount, 1)



