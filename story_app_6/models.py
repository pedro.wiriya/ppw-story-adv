from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    name = models.CharField(max_length=100)
    date = models.DateField()

    def __str__(self):
        return self.name

class Orang(models.Model):
    full_name = models.CharField(max_length=100)
    nickname = models.CharField(max_length=20)
    gender_choice = [
        ('Male', 'Male'),
        ('Female', 'Female')
    ]
    gender = models.CharField(max_length=9, choices=gender_choice)
    birthday = models.DateField()
    activity = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    def __str__(self):
        return self.full_name