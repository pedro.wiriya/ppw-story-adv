from django.db.models import fields
from django import forms
from django.forms import ModelForm, TextInput, DateInput
from django.forms import widgets
from .models import Orang, Kegiatan
import datetime

gender = (('Male', 'Male'),('Female','Female'))
class OrangForm(ModelForm):
    class Meta:
        model = Orang
        fields = [
            'full_name',
            'nickname',
            'gender',
            'birthday',
            'activity'
        ]
        widgets = {
            'full_name' : TextInput(attrs={'class':'form-control form-control-sm', 'placeholder':'Enter your full name here', 'required': True}),
            'nickname' : TextInput(attrs={'class':'form-control form-control-sm' ,'placeholder' : 'Enter your nickname here', 'required': True}),
            'gender' : forms.Select(choices=gender, attrs={'class':'form-control form-control-sm', 'required': True}),
            'birthday' : DateInput(attrs={'class':'form-control form-control-sm' ,'placeholder' : 'YYYY-MM-DD', 'type' : 'date', 'min' : '1990-01-01', 'max' : '2020-12-31','required':True})
        }

class KegiatanForm(ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'name',
            'date'
        ]
        widgets = {
            'name' : TextInput(attrs={'class':'form-control form-control-sm','placeholder':'Your name please', 'required' : True}),
            'date' : DateInput(attrs={'class':'form-control form-control-sm','placeholder' : 'YYYY-MM-DD','type' : 'date', 'min' : '1990-01-01', 'max' : '2020-12-31','required' : True})
        }