from django.test import TestCase, Client
from django.urls import resolve
from .views import accord
from story_app_7.apps import StoryApp7Config

class Story7Test(TestCase):

    def test_url(self):
        response = Client().get('/my-accord/')
        self.assertEquals(response.status_code, 200)

    def test_views(self):
        response = resolve('/my-accord/')
        self.assertEquals(response.func, accord)

    def test_templates(self):
        response = Client().get('/my-accord/')
        self.assertTemplateUsed(response, 'accordion.html')

    def test_app(self):
        self.assertEquals(StoryApp7Config.name, 'story_app_7')
