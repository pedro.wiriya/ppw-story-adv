from django.urls import path
from .views import accord

urlpatterns = [
    path('my-accord/', accord, name="accord")
]
