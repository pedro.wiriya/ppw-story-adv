from django.http.response import JsonResponse
from django.shortcuts import render
import urllib.request, json

def search(request):
    return render(request, 'searchbook.html')

def search_api(request, title):
    link = urllib.parse.quote("https://www.googleapis.com/books/v1/volumes?q="+ title, "/:?=")
    with urllib.request.urlopen(link) as url:
        global data
        data = json.loads(url.read().decode())
    return JsonResponse(data)
