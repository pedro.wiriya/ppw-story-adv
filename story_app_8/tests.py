from django.test import TestCase, Client
from django.urls import resolve
from .views import search, search_api

class Story8Test(TestCase):

    def test_search_url(self):
        rsp = Client().get('/search/')
        self.assertEqual(rsp.status_code, 200)

    def test_api_url(self):
        rsp = Client().get('/search/Genshin%20Impact/')
        self.assertEqual(rsp.status_code, 200)

    def test_search_views(self):
        rsp = resolve('/search/')
        self.assertEqual(rsp.func, search)

    def test_api_views(self):
        rsp = resolve('/search/Genshin%20Impact/')
        self.assertEqual(rsp.func, search_api)

    def test_template(self):
        rsp = Client().get('/search/')
        self.assertTemplateUsed(rsp, 'searchbook.html')
