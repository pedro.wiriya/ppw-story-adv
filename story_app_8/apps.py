from django.apps import AppConfig


class StoryApp8Config(AppConfig):
    name = 'story_app_8'
