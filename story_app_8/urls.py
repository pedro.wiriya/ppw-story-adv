from django.urls import path
from .views import search, search_api

urlpatterns = [
    path('search/', search, name='search'),
    path('search/<str:title>/', search_api)
]
